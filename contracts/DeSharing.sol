pragma solidity ^0.5.0;

contract DeSharing {

  //Model an uploaded object
  struct UploadedObject{
    string uploadedHash;
    address owner;
    address recipient;
  }

  //Model a permission object
  struct PermissionObject{
    address recipient;
    string publicKey;
  }

  //Stores accounts that want permission to a file
  mapping(address=>PermissionObject[]) public forPermission;

  //Stores the sharings
  mapping(address=>UploadedObject[]) public givedPermission;

  //Stores the uploads
  mapping(address=>UploadedObject[]) public uploads;

  address[] forPermissionKeysNames;
  address[] givedPermissionKeysNames;
  address[] uploadsKeysNames;

  function set1(address key, address recipientValue, string memory publicKeyValue) public{
    if(forPermission[key].length == 0){
      forPermissionKeysNames.push(key);
    }

    forPermission[key].push(PermissionObject(recipientValue, publicKeyValue));
  }

  function get1(address key, uint index) view public returns(string memory, address){

    return (forPermission[key][index].publicKey, forPermission[key][index].recipient);
  }


  function get2(address key, uint index) view public returns(string memory, address, address){

    return (givedPermission[key][index].uploadedHash, givedPermission[key][index].owner, givedPermission[key][index].recipient);
  }

  function get3(address key, uint index) view public returns(string memory){

    return uploads[key][index].uploadedHash;
  }

  function num_of(address key, uint i) view public returns(uint) {
    if(i == 1){
      return forPermission[key].length;
    }else if(i == 2){
      return givedPermission[key].length;
    }else if(i == 3){
      return uploads[key].length;
    }
  }

  function set2(address _fileRecipientValue, address _ownValue, string memory _sharedUploadedHashValue) public{
    if(givedPermission[_fileRecipientValue].length == 0){
      givedPermissionKeysNames.push(_fileRecipientValue);
    }

    givedPermission[_fileRecipientValue].push(UploadedObject(_sharedUploadedHashValue, _ownValue, _fileRecipientValue));
  }

  function set3(address _whoValue, address _ownValue, string memory _sharedUploadedHashValue) public{
    if(uploads[_whoValue].length == 0){
      uploadsKeysNames.push(_whoValue);
    }

    uploads[_whoValue].push(UploadedObject(_sharedUploadedHashValue, _ownValue, _whoValue));
  }

  //permission event
  event permissionEvent(
    address indexed _permissionSender,
    address indexed _permissionRecipient
  );

  //uploaded event
  event uploadEvent(
    address indexed _who,
    string uploadedHash
  );

  //sharing event
  event sharingEvent(
    address indexed _sharingRecipient,
    address indexed _sharingOwner,
    string _sharinguploadedHash
  );

  constructor() public {
  }

  //a function for uploading a file
  function uploadF(address _who, string memory _sharedUploadedHash) public{
    set3(_who, msg.sender, _sharedUploadedHash);

    //trigger upload event
    emit uploadEvent(_who, _sharedUploadedHash);
  }


  //a function that allows users to request permission to an owner's file
  function requestPermission(address _owner, string memory _publicKey) public{
    set1(_owner, msg.sender, _publicKey);

    //trigger permission event
    emit permissionEvent(msg.sender, _owner);
  }

  //function that gives permission to a file, providing the uploaded hash to swarm and the shared key to decrypt the file
  function givePermission(address _fileRecipient, string memory _sharedUploadedHash) public{
    set2(_fileRecipient, msg.sender, _sharedUploadedHash);

    //trigger sharing event
    emit sharingEvent(_fileRecipient, msg.sender, _sharedUploadedHash);
  }
}
