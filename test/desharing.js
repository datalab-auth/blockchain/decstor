var DeSharing = artifacts.require("./DeSharing.sol");


contract("DeSharing", function(accounts){

  var anInstance;

  it("uploadF", function(){
    return DeSharing.deployed().then(function(instance){
      anInstance = instance;
      return anInstance.uploadF(accounts[0], "b710daad766d410477950609d9524f39022f2412652467166b501ccd08169f2c", {from: accounts[0]});
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 1, "an event was triggered");
      assert.equal(receipt.logs[0].event, "uploadEvent", "the event type is correct");
      assert.equal(receipt.logs[0].args.uploadedHash.toString(), "b710daad766d410477950609d9524f39022f2412652467166b501ccd08169f2c", "the uploaded hash is correct");
      return anInstance.num_of(accounts[0], 3);
    }).then(function(number){
      assert.equal(number, 1, "right");
      return anInstance.get3(accounts[0], 0);
    }).then(function(hash){
      assert.equal(hash, "b710daad766d410477950609d9524f39022f2412652467166b501ccd08169f2c", "the hash is correct");
    })
  });

  it("requestPermission", function(){
    return DeSharing.deployed().then(function(instance){
      anInstance = instance;
      return anInstance.requestPermission(accounts[0], "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDlOJu6TyygqxfWT7eLtGDwajtNFOb9I5XRb6khyfD1Yt3YiCgQWMNW649887VGJiGr/L5i2osbl8C9+WJTeucF+S76xFxdU6jE0NQ+Z+zEdhUTooNRaY5nZiu5PgDB0ED/ZKBUSLKL7eibMxZtMlUDHjm4gwQco1KRMDSmXSMkDwIDAQAB", {from: accounts[0]});
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 1, "an event was triggered");
      assert.equal(receipt.logs[0].event, "permissionEvent", "the event type is correct");
      assert.equal(receipt.logs[0].args._permissionSender.toString(), accounts[0], "the public key hash is correct");
      return anInstance.num_of(accounts[0], 1);
    }).then(function(number){
      assert.equal(number, 1, "right");
      return anInstance.get1(accounts[0], 0);
    }).then(function(pubkey){
      assert.equal(pubkey[0], "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDlOJu6TyygqxfWT7eLtGDwajtNFOb9I5XRb6khyfD1Yt3YiCgQWMNW649887VGJiGr/L5i2osbl8C9+WJTeucF+S76xFxdU6jE0NQ+Z+zEdhUTooNRaY5nZiu5PgDB0ED/ZKBUSLKL7eibMxZtMlUDHjm4gwQco1KRMDSmXSMkDwIDAQAB", "the hash is correct");
    })
  });

  it("givePermission", function(){
    return DeSharing.deployed().then(function(instance){
      anInstance = instance;
      return anInstance.givePermission(accounts[1], "b710daad766d410477950609d9524f39022f2412652467166b501ccd08169f2c", {from: accounts[0]});
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 1, "an event was triggered");
      assert.equal(receipt.logs[0].event, "sharingEvent", "the event type is correct");
      assert.equal(receipt.logs[0].args._sharinguploadedHash.toString(), "b710daad766d410477950609d9524f39022f2412652467166b501ccd08169f2c", "the hash is correct");
      return anInstance.num_of(accounts[1], 2);
    }).then(function(number){
      assert.equal(number, 1, "right");
      return anInstance.get2(accounts[1], 0);
    }).then(function(hash){
      assert.equal(hash[0], "b710daad766d410477950609d9524f39022f2412652467166b501ccd08169f2c", "the hash is correct");
    })
  });
});
