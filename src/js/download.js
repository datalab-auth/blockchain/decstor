Download = {
  web3Provider: null,
  contracts: {},
  account: '0x0',

  init: function(){
    return Download.initWeb3();
  },

  initWeb3: function(){
    if(window.ethereum){
      Download.web3Provider = web3.currentProvider;
      web3 = new Web3(window.ethereum);
    }
    else if(window.web3) {
      Download.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    }
    else {
      console.log('You have to install MetaMask !')
      // Specify default instance if no web3 instance provided
      Download.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      web3 = new Web3(Download.web3Provider);
    }

    return Download.initContract();
  },

  initContract: function(){
    $.getJSON("DeSharing.json", function(sharing){
      // Instantiate a new truffle contract from the artifact
      Download.contracts.DeSharing = TruffleContract(sharing);
      // Connect provider to interact with contract
      Download.contracts.DeSharing.setProvider(Download.web3Provider);

      //listen for events
      Download.listenForShareEvent();

      return Download.render();

    });
  },

  // Listen for events of uploading emitted from the contract
  listenForShareEvent: function(){
    Download.contracts.DeSharing.deployed().then(function(instance){
      instance.sharingEvent({_sharingRecipient: Download.account}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event)

      });
    });
  },

  render: function(){
    var downInstance;

    window.ethereum.on('accountsChanged', function (accounts) {
      location.reload();
    });

    //Load account data
    web3.eth.getCoinbase(function(err, account){
      if(err === null){
        Download.account = account;
        $("#accountAddress").html("Your Account: " + account);
      }
    });

    //Load contract data
    Download.contracts.DeSharing.deployed().then(function(instance){
      downInstance = instance;
      return downInstance.num_of(Download.account, 2);
    }).then(function(downsCount){
      console.log(downsCount)
      var downloaded = $("#downloaded");
      downloaded.empty();

      var title = "<tr><th>" + "Hash" + "</th></tr>" + "<tr><th>" + "Owner" + "</th></tr>" + "<tr><th>" + "Recipient" + "</th></tr>"
      downloaded.append(title);

      for(var i=0; i<= downsCount.c[0]-1; i++){

        downInstance.get2(Download.account, i).then(function(down){

          //Render uploads
          var downsTemplate = "<tr><th>" + down[0] + "</th></tr>" + "<tr><th>" + down[1] + "</th></tr>" + "<tr><th>" + down[2] + "</th></tr>"
          downloaded.append(downsTemplate);

        });
      }
    }).then(function(){
      // loader.hide();
      // content.show();
    }).catch(function(error){
      console.log(error);
    });
  },

  downloadData: function(){

    const client = new Erebos.SwarmClient({
      http: 'https://swarm-gateways.net',
    })

    var dec = new JSEncrypt();
    dec.setPrivateKey(document.getElementById("privatekey").value);

    hash = document.getElementById("filehash").value

    client.bzz.download(hash)
    .then(res => res.text())
    .then(text => {
      myText = JSON.parse(text);

      var uncrypted = dec.decrypt(myText.shareKey);

      var sdata = decrypt_file(myText.encData, uncrypted);

      var blob = new Blob([sdata], {type: "text/plain;charset=utf-8"});
      saveAs(blob, hash.concat(".txt"));

    }).catch(function(error){
      console.log(error)
    });
  },
};

$(function(){
  $(window).load(function(){
    Download.init();
  });
});
