
//Encrypt the file with a random generated File-key
encrypt_file = function(fileContents){

  //Generate File-key
  var idRandom = '';
  var array = new Uint8Array(32);
  let idValues = window.crypto.getRandomValues(array);

  var len = idValues.byteLength;
  for(var i = 0; i < len; i++){
    idRandom += String.fromCharCode(idValues[i]);
  }

  id = window.btoa(idRandom);

  let ciphertext = CryptoJS.AES.encrypt(fileContents.toString(), id);

  var rObj = {
    "fileKey": id,
    "ciphertext": ciphertext.toString()
  }

  return rObj;
};

//Decrypt the file using the File-key
decrypt_file = function(ciphertext, id){

  let bytes = CryptoJS.AES.decrypt(ciphertext, id);
  let plaintext = bytes.toString(CryptoJS.enc.Utf8);

  return plaintext
};
