AskPermission = {
  web3Provider: null,
  contracts: {},
  account: '0x0',

  init: function(){
    return AskPermission.initWeb3();
  },

  initWeb3: function(){
    if(window.ethereum){
      AskPermission.web3Provider = web3.currentProvider;
      web3 = new Web3(window.ethereum);
    }
    else if(window.web3) {
      AskPermission.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    }
    else {
      console.log('You have to install MetaMask !')
      // Specify default instance if no web3 instance provided
      AskPermission.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      web3 = new Web3(Upload.web3Provider);
    }

    return AskPermission.initContract();
  },

  initContract: function(){
    $.getJSON("DeSharing.json", function(sharing){
      // Instantiate a new truffle contract from the artifact
      AskPermission.contracts.DeSharing = TruffleContract(sharing);
      // Connect provider to interact with contract
      AskPermission.contracts.DeSharing.setProvider(AskPermission.web3Provider);

      //listen for events
      AskPermission.listenForPermissionEvent();

      return AskPermission.render();

    });
  },

  // Listen for events of uploading emitted from the contract
  listenForPermissionEvent: function(){
    AskPermission.contracts.DeSharing.deployed().then(function(instance){
      instance.permissionEvent({_permissionSender: AskPermission.account}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event)
      });
    });
  },

  render: function(){
    var askInstance;

    window.ethereum.on('accountsChanged', function (accounts) {
      location.reload();
    });

    //Load account data
    web3.eth.getCoinbase(function(err, account){
      if(err === null){
        AskPermission.account = account;
        $("#accountAddress").html("Your Account: " + account);
      }
    });

    //Load contract data
    AskPermission.contracts.DeSharing.deployed().then(function(instance){
      askInstance = instance;
      return askInstance.num_of(AskPermission.account, 1);
    }).then(function(asksCount){
      var asked = $("#asked");
      asked.empty();

      var title = "<tr><th>" + "Recipient's Public Key" + "</th></tr>" + "<tr><th>" + "Recipient's Address" + "</th></tr>"
      asked.append(title);

      for(var i=0; i<= asksCount.c[0]-1; i++){

        askInstance.get1(AskPermission.account, i).then(function(up){


          //Render asks
          var asksTemplate = "<tr><th>" + up[0] + "</th></tr>" + "<tr><th>" + up[1] + "</th></tr>"
          asked.append(asksTemplate);

        });
      }
    }).then(function(){
      // loader.hide();
      // content.show();
    }).catch(function(error){
      console.log(error);
    });
  },

  permissionAsking: function(){
    AskPermission.contracts.DeSharing.deployed().then(function(instance){
      return instance.requestPermission(web3.utils.toChecksumAddress(document.getElementById("permissionRecipient").value), document.getElementById("pubkey").value, {from: AskPermission.account});

    }).then(function(result){
      // AskPermission.render();
    }).catch(function(error){
      console.log(error)
    });
  },
};

$(function(){
  $(window).load(function(){
    AskPermission.init();
  });
});
