Share = {
  web3Provider: null,
  contracts: {},
  account: '0x0',
  shareKey: '',
  encryptedDt:'',


  init: function(){
    return Share.initWeb3();
  },

  initWeb3: function(){
    if(window.ethereum){
      Share.web3Provider = web3.currentProvider;
      web3 = new Web3(window.ethereum);
    }
    else if(window.web3) {
      Share.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    }
    else {
      console.log('You have to install MetaMask !')
      // Specify default instance if no web3 instance provided
      Share.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      web3 = new Web3(Share.web3Provider);
    }

    return Share.initContract();
  },

  initContract: function(){
    $.getJSON("DeSharing.json", function(sharing){
      // Instantiate a new truffle contract from the artifact
      Share.contracts.DeSharing = TruffleContract(sharing);
      // Connect provider to interact with contract
      Share.contracts.DeSharing.setProvider(Share.web3Provider);

      //listen for events
      Share.listenForShareEvent();

      Share.listenForPermissionEvent();

      Share.listenForUploadEvent();

      return Share.render();

    });
  },

  // Listen for events of uploading emitted from the contract
  listenForShareEvent: function(){
    Share.contracts.DeSharing.deployed().then(function(instance){
      instance.sharingEvent({_sharingROwner: Share.account}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event)

      });
    });
  },

  listenForPermissionEvent: function(){
    Share.contracts.DeSharing.deployed().then(function(instance){
      instance.permissionEvent({_permissionSender: Share.account}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event)


      });
    });
  },

  listenForUploadEvent: function(){
    Share.contracts.DeSharing.deployed().then(function(instance){
      instance.uploadEvent({filter: {_who: Share.account}}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event.args.uploadedHash)


      });
    });
  },

  render: function(){
    var shareInstance;

    window.ethereum.on('accountsChanged', function (accounts) {
      location.reload();
    });

    //Load account data
    web3.eth.getCoinbase(function(err, account){
      if(err === null){
        Share.account = account;
        $("#accountAddress").html("Your Account: " + account);
      }
    });


    //Load contract data
    Share.contracts.DeSharing.deployed().then(function(instance){
      shareInstance = instance;
      return shareInstance.num_of(Share.account, 2);
    }).then(function(sharesCount){
      var shared = $("#shared");
      shared.empty();

      var title = "<tr><th>" + "Hash" + "</th></tr>" + "<tr><th>" + "Owner" + "</th></tr>" + "<tr><th>" + "Recipient" + "</th></tr>"
      shared.append(title);

      for(var i=0; i<= sharesCount.c[0]-1; i++){

        shareInstance.get2(Share.account, i).then(function(up){

          console.log(up)
          //Render uploads
          var sharesTemplate = "<tr><th>" + up[0] + "</th></tr>" + "<tr><th>" + up[1] + "</th></tr>" + "<tr><th>" + up[2] + "</th></tr>"
          shared.append(sharesTemplate);

        });
      }
      return shareInstance.num_of(Share.account, 1);
    }).then(function(asksCount){
      var asked = $("#notpermissioned");
      asked.empty();

      var title = "<tr><th>" + "Recipient's Public Key" + "</th></tr>" + "<tr><th>" + "Recipient's Address" + "</th></tr>"
      asked.append(title);

      for(var i=0; i<= asksCount.c[0]-1; i++){
        shareInstance.get1(Share.account, i).then(function(up){


          //Render asks
          var asksTemplate = "<tr><th>" + up[0] + "</th></tr>" + "<tr><th>" + up[1] + "</th></tr>"
          asked.append(asksTemplate);

        });
      }
      return shareInstance.num_of(Share.account, 3);
    }).then(function(uploadsCount){
      var uploaded = $("#uploaded");
      uploaded.empty();

      var title = "<tr><th>" + "Hash" + "</th></tr>"
      uploaded.append(title);

      for(var i=0; i<= uploadsCount.c[0]-1; i++){
        shareInstance.get3(Share.account, i).then(function(up){
          //Render uploads
          var uploadsTemplate = "<tr><th>" + up + "</th></tr>"
          uploaded.append(uploadsTemplate);

        }).catch(function(er){
          console.log(er)
        });
      }
    }).catch(function(error){
      console.log(error);
    });
  },

  shareData: function(){

    const client = new Erebos.SwarmClient({
      http: 'https://swarm-gateways.net',
    })

    var dec = new JSEncrypt();
    dec.setPrivateKey(document.getElementById("privatekey").value);

    client.bzz.download(document.getElementById("filehash").value)
    .then(res => res.text())
    .then(text => {
      myText = JSON.parse(text);
      var uncrypted = dec.decrypt(myText.shareKey);

      var sdata = decrypt_file(myText.encData, uncrypted);


      rObj = encrypt_file(sdata);
      Share.encryptedDt = rObj.ciphertext;

      var enc = new JSEncrypt();
      enc.setPublicKey(document.getElementById("pubkey").value);
      Share.shareKey = enc.encrypt(rObj.fileKey);

      let sData = {
        shareKey: Share.shareKey,
        encData: Share.encryptedDt
      };

      var stData = JSON.stringify(sData);

      client.bzz.upload(stData, {contentType: 'text/plain'}).then((hash) => {

        Share.contracts.DeSharing.deployed().then(function(instance){
          return instance.givePermission(web3.utils.toChecksumAddress(document.getElementById("recipientAd").value), hash, {from: Share.account});
        }).then(function(result){
          Share.render();
        }).catch(function(e){
          console.log(e)
        });
      }).catch(function(err){
        console.log(err)
      });
    }).catch(function(error){
      console.log(error)
    });
  },
};

$(function(){
  $(window).load(function(){
    Share.init();
  });
});
