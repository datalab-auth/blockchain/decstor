Upload = {
  web3Provider: null,
  contracts: {},
  account: '0x0',
  fileContent: '',
  shareKey: '',
  encryptedDt: '',

  init: function(){
    return Upload.initWeb3();
  },

  initWeb3: function(){
    if(window.ethereum){
      Upload.web3Provider = web3.currentProvider;
      web3 = new Web3(window.ethereum);
    }
    else if(window.web3) {
      Upload.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    }
    else {
      console.log('You have to install MetaMask !')
      // Specify default instance if no web3 instance provided
      Upload.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');
      web3 = new Web3(Upload.web3Provider);
    }

    return Upload.initContract();
  },

  initContract: function(){
    $.getJSON("DeSharing.json", function(sharing){
      // Instantiate a new truffle contract from the artifact
      Upload.contracts.DeSharing = TruffleContract(sharing);
      // Connect provider to interact with contract
      Upload.contracts.DeSharing.setProvider(Upload.web3Provider);

      //listen for events
      Upload.listenForUploadEvent();

      return Upload.render();

    });
  },

  // Listen for events of uploading emitted from the contract
  listenForUploadEvent: function(){
    Upload.contracts.DeSharing.deployed().then(function(instance){
      instance.uploadEvent({filter: {_who: Upload.account}}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event.args.uploadedHash)

        // location.reload();
      });
    });
  },

  render: function(){
    var uploadInstance;

    window.ethereum.on('accountsChanged', function (accounts) {
      location.reload();
    });
    // var loader = $("#loader");
    // var content = $("#content");
    //
    //
    // loader.show();
    // content.hide();

    //Load account data
    web3.eth.getCoinbase(function(err, account){
      if(err === null){
        Upload.account = account;
        $("#accountAddress").html("Your Account: " + account);
      }
    });

    //Load contract data
    Upload.contracts.DeSharing.deployed().then(function(instance){
      uploadInstance = instance;
      return uploadInstance.num_of(Upload.account, 3);
    }).then(function(uploadsCount){
      var uploaded = $("#uploaded");
      uploaded.empty();

      var title = "<tr><th>" + "Hash" + "</th></tr>"
      uploaded.append(title);

      for(var i=0; i<= uploadsCount.c[0]-1; i++){
        uploadInstance.get3(Upload.account, i).then(function(up){
          //Render uploads
          var uploadsTemplate = "<tr><th>" + up + "</th></tr>"
          uploaded.append(uploadsTemplate);

        }).catch(function(er){
          console.log(er)
        });
      }
    }).catch(function(error){
      console.log(error);
    });

    document.getElementById('fileupload').addEventListener('change',function(evt) {
      var file = evt.target.files[0];

      if (file) {
        new Promise(function(resolve, reject) {
          var reader = new FileReader();
          reader.onload = function (e) {
            resolve(e.target.result);
          };
          reader.readAsText(file);
          reader.onerror = reject;
        })
        .then((data) => {Upload.fileContent = data
        console.log(data)})
        .catch(function(err) {
          console.log(err)
        });
      }
    }, false);

  },

  uploadData: function(){

    rObje = encrypt_file(Upload.fileContent);
    Upload.encryptedDt = rObje.ciphertext;

    var enc = new JSEncrypt();

    enc.setPublicKey(document.getElementById("pubkey").value);
    Upload.shareKey = enc.encrypt(rObje.fileKey);

    let data = {
      shareKey: Upload.shareKey,
      encData: Upload.encryptedDt
    };

    var stData = JSON.stringify(data);

    const swarm = new Erebos.SwarmClient({
      http: 'https://swarm-gateways.net',
    })

    swarm.bzz.upload(stData, {contentType: 'text/plain'}).then((hash) => {

      Upload.contracts.DeSharing.deployed().then(function(instance){
        return instance.uploadF(Upload.account, hash);

      }).then(function(result){
        // Upload.render();
      }).catch(function(error){
        console.log(error)
      });
    }).catch(function(error){
      console.log(error)
    });
  },
};

$(function(){
  $(window).load(function(){
    Upload.init();
  });
});
